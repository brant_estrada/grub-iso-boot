## [GRUB](https://www.gnu.org/software/grub/) script to boot various Linux live CD/DVD images

This script is based on https://github.com/Jimmy-Z/grub-iso-boot and credit to the grub.cfg file goes to Jimmy-z

### Usage:
0. Ensure that your USB drive doesnt have any important files as it **will** be formatted in its entirety
1. Run the script with the path to your usb drive as arguments
2. Put image files in `(USB)/boot/iso/`.
3. Boot from this device, that depends of your system/motherboard vendor.
	- On some modern PCs, you can press F12 to select from a list of boot devices during POST.
	- Configure BIOS/EFI to boot from this device.
	- [More detailed document from Debian](https://www.debian.org/releases/stable/amd64/ch03s06.html.en#boot-dev-select)

### Further Information:
For currently supported distros, or how the grub.cfg file works, see the original author [here](https://github.com/Jimmy-Z/grub-iso-boot)

